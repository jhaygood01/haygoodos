//This section was taken from www.osdev.com tutorial
//---------------------------------------------------------------------------
#if !defined(__cplusplus)
#include <stdbool.h> /* C doesn't have booleans by default. */
#endif
#include <stddef.h>
#include <stdint.h>
#include "include/drivers/ps2.h"
#include "include/drivers/portio.h"
#include "include/drivers/vga.h"
#include "include/drivers/CMOS.h"
#include "include/drivers/PIT.h"
#include "include/terminal.h"
#include "include/strings.h"
#include "include/math.h"
/* Check if the compiler thinks we are targeting the wrong operating system. */
#if defined(__linux__)
#error "You are not using a cross-compiler, you will most certainly run into trouble"
#endif
#if !defined(__i386__)
#error "This tutorial needs to be compiled with a ix86-elf compiler"
#endif
#if defined(__cplusplus)
extern "C" /* Use C linkage for kernel_main. */
#endif
//                              END SECTION
//---------------------------------------------------------------------------
void byteBits(int byte)
{
    unsigned char mask = 1; // Bit mask
    unsigned char bits[8];
    int i, j = 7;
    // Extract the bits
    for ( i = 0; i < 8; i++,j--,mask = 1) {
        // Mask each bit in the byte and store it
        bits[i] =( byte & (mask<<=j))  != NULL;
    }
    // For debug purposes, lets print the received data
    for (int i = 0; i < 8; i++) {
        if(bits[i] == 1)
        {
            terminal_writestring("0");
        }else{
            terminal_writestring("1");
        }
    }
}
void clear(char* buffer, int len)
{
    for(int i = 0; i < len; i++)
    {
        buffer[i] = '\0';
    }
}
void kernel_main(void) {
    engageTerminalLoop();
}

