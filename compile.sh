export PREFIX="$HOME/opt/cross"
export TARGET=i686-elf
export PATH="$PREFIX/bin:$PATH"
clear
i686-elf-as boot.s -o bin/boot.o -w
i686-elf-gcc -c kernel.c -o bin/kernel.o -std=gnu99 -ffreestanding -O2 -Wall -Wextra -w
i686-elf-gcc -T linker.ld -o bin/myos.bin -ffreestanding -O2 -nostdlib bin/boot.o bin/kernel.o -lgcc -w
mkdir -p isodir/boot/grub
cp bin/myos.bin isodir/boot/myos.bin
cp bin/grub.cfg isodir/boot/grub/grub.cfg
rm -R /volumes/boot\ 4/isodir
cp -r isodir /volumes/boot\ 4
git add --all

echo "$would you like to commit changes?"
read response
if [[ "$response" =~ [Yy]$ ]]
then
   echo "$Please enter a commit message: "
   read message
   git commit -m "\"$message\""
   echo "\"$message\""
   echo "Push to Bitbucket repo? [Y,N]"
   read response
   if [[ "$response" =~ [Yy]$ ]]
   then
      git push -u origin master
   fi
fi


