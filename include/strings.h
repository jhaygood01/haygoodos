//                                        STRING MANIPULATION LIBRARY
//                                        CREATED BY JAKE HAYGOOD 2017
//------------------------------------------------------------------------------------------------------------------------
//NOTE: THERE ARE NO BOOLEAN RETURN TYPES BECAUSE THEY ARE NOT WOKRING PROPERLY. SUBSTITUTING INT AND USING ONE OR ZERO
#ifndef strings_h
#define strings_h
int startsWith(const char* string, const char* start, int length)
{
    bool flag = 1;
    for(int i = 0; i < length; i++)
    {
        if(!(string[i] == start[i]))
        {
            flag = 0;
        }
    }
    return flag;
}



void intToString(char* output, int number)
{
    int inc = getDigits(number);
    for(int i = 0; i < getDigits(number); i++)
    {
        output[i] = "_";
    }
    while(number > 0)
    {
        switch(number%10)
        {
            case 0:
                output[inc] = '0';
                break;
            case 1:
                output[inc] = '1';
                break;
            case 2:
                output[inc] = '2';
                break;
            case 3:
                output[inc] = '3';
                break;
            case 4:
                output[inc] = '4';
                break;
            case 5:
                output[inc] = '5';
                break;
            case 6:
                output[inc] = '6';
                break;
            case 7:
                output[inc] = '7';
                break;
            case 8:
                output[inc] = '8';
                break;
            case 9:
                output[inc] = '9';
                break;
            default:
                break;
                
        }
        inc -= 1;
        number = number / 10;
    }
    
}

int stringToInt(const char* s, int digits)
{
    int output = 0;
    int cur;
    int carry = 0;
    for(int i = digits; i > 0; i--)
    {
        switch(s[i-1])
        {
            case '0':
                cur = 0;
                break;
            case '1':
                cur = 1;
                break;
            case '2':
                cur = 2;
                break;
            case '3':
                cur = 3;
                break;
            case '4':
                cur = 4;
                break;
            case 5:
                cur = 5;
                break;
            case 6:
                cur = 6;
                break;
            case 7:
                cur = 7;
                break;
            case 8:
                cur = 8;
                break;
            case 9:
                cur = 9;
                break;
                
                
        }
        if(cur == 0)
        {
            
        }else{
            output += cur*(power(10, digits-i));
        }
        
    }
    return output;
}

#endif /* strings_h */
