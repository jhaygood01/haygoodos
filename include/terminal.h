//                                              TERMINAL MAIN LOOP
//                                        CREATED BY JAKE HAYGOOD 2017
//------------------------------------------------------------------------------------------------------------------------
#ifndef terminal_h
#define terminal_h
void engageTerminalLoop()
{
    terminal_initialize();
    char* sysprompt = "SYS>";
    char buffer[128];
    bool commandFlag;
    while(true)
    {
        commandFlag = false;
        terminal_writestring(sysprompt);
        readLine(buffer);
        newline();
        if(startsWith(buffer, "clear", 5) == 1)
        {
            terminal_clear();
            terminal_column = 0;
            terminal_row = 0;
            commandFlag = true;
        }
        if(startsWith(buffer, "info", 5) == 1)
        {
            
            commandFlag=true;
        }
        clear(buffer, 128);
     }
}
            
    
           
#endif /* terminal_h */
