
#include "portio.h"
#include "vga.h"
#ifndef __h
#define __h
//                                                      PS/2 DRIVER
//                                              CREATED BY JAKE HAYGOOD 2017
//------------------------------------------------------------------------------------------------------------------------
enum KEYBOARD_MAP {
    US1,
    US2,
};
const char* loop_read_character(enum KEYBOARD_MAP map)
{
    while(1)
    {
        
        int ref = inb(0x60);
        switch(map)
        {
            default:
            case US1:
                switch(ref)
            {
                case 0x1e:
                    return 'a';
                    break;
                case 0x30:
                    return 'b';
                    break;
                case 0x2E:
                    return 'c';
                    break;
                case 0x20:
                    return 'd';
                    break;
                case 0x12:
                    return 'e';
                    break;
                case 0x21:
                    return 'f';
                    break;
                case 0x22:
                    return 'g';
                    break;
                case 0x23:
                    return 'h';
                    break;
                case 0x17:
                    return 'i';
                    break;
                    
                case 0x24:
                    return 'j';
                    break;
                    
                case 0x25:
                    return 'k';
                    break;
                    
                case 0x26:
                    return 'l';
                    break;
                    
                case 0x32:
                    return 'm';
                    break;
                    
                case 0x31:
                    return 'n';
                    break;
                    
                case 0x18:
                    return 'o';
                    break;
                    
                case 0x19:
                    return 'p';
                    break;
                    
                case 0x10:
                    return 'q';
                    break;
                    
                case 0x13:
                    return 'r';
                    break;
                    
                case 0x1F:
                    return 's';
                    break;
                    
                case 0x14:
                    return 't';
                    break;
                    
                case 0x16:
                    return 'u';
                    break;
                    
                case 0x2F:
                    return 'v';
                    break;
                    
                case 0x11:
                    return 'w';
                    break;
                    
                case 0x2D:
                    return 'x';
                    break;
                    
                case 0x15:
                    return 'y';
                    break;
                    
                case 0x2C:
                    return 'z';
                    break;
                    
                case 0x0B:
                    return '0';
                    break;
                    
                case 0x02:
                    return '1';
                    break;
                    
                case 0x03:
                    return '2';
                    break;
                    
                case 0x04:
                    return '3';
                    break;
                    
                case 0x05:
                    return '4';
                    break;
                    
                case 0x06:
                    return '5';
                    break;
                    
                case 0x07:
                    return '6';
                    break;
                    
                case 0x08:
                    return '7';
                    break;
                    
                case 0x09:
                    return '8';
                    break;
                    
                case 0x0a:
                    return '9';
                    break;
                    
                case 0x29:
                    return '`';
                    break;
                    
                case 0x0c:
                    return '-';
                    break;
                    
                case 0x0d:
                    return '=';
                    break;
                    
                case 0x2b:
                    return '\\';
                    break;
                    
                case 0x0e:
                    return 0x0e;
                    break;
                    
                case 0x39:
                    return ' ';
                    break;
                    
                case 0x0f:
                    return '   ';
                    break;
                    
                case 0x1C:
                    return 0x1C;
                    break;

        }
        
    }
    
}
}
void readLine(char* buffer)
{
    bool running = true;
    int inc = 0;
    while(running)
    {
    char curchar = loop_read_character(US1);
    switch(curchar)
    {
        case 'a':
            terminal_writestring("a");
            buffer[inc] = 'a';
            break;
        case 'b':
            terminal_writestring("b");
            buffer[inc] = 'b';
             break;
        case 'c':
            terminal_writestring("c");
            buffer[inc] = 'c';
             break;
        case 'd':
            terminal_writestring("d");
            buffer[inc] = 'd';
             break;
        case 'e':
            terminal_writestring("e");
            buffer[inc] = 'e';
             break;
        case 'f':
            terminal_writestring("f");
            buffer[inc] = 'f';
             break;
        case 'g':
            terminal_writestring("g");
            buffer[inc] = 'g';
             break;
        case 'h':
            terminal_writestring("h");
            buffer[inc] = 'h';
             break;
        case 'i':
            terminal_writestring("i");
            buffer[inc] = 'i';
             break;
        case 'j':
            terminal_writestring("j");
            buffer[inc] = 'j';
             break;
        case 'k':
            terminal_writestring("k");
            buffer[inc] = 'k';
             break;
        case 'l':
            terminal_writestring("l");
            buffer[inc] = 'l';
             break;
        case 'm':
            terminal_writestring("m");
            buffer[inc] = 'm';
             break;
        case 'n':
            terminal_writestring("n");
            buffer[inc] = 'n';
             break;
        case 'o':
            terminal_writestring("o");
            buffer[inc] = 'o';
             break;
        case 'p':
            terminal_writestring("p");
            buffer[inc] = 'p';
             break;
        case 'q':
            terminal_writestring("q");
            buffer[inc] = 'q';
             break;
        case 'r':
            terminal_writestring("r");
            buffer[inc] = 'r';
             break;
        case 's':
            terminal_writestring("s");
            buffer[inc] = 's';
             break;
        case 't':
            terminal_writestring("t");
            buffer[inc] = 't';
             break;
        case 'u':
            terminal_writestring("u");
            buffer[inc] = 'u';
             break;
        case 'v':
            terminal_writestring("v");
            buffer[inc] = 'v';
             break;
        case 'w':
            terminal_writestring("w");
            buffer[inc] = 'w';
             break;
        case 'x':
            terminal_writestring("x");
            buffer[inc] = 'x';
             break;
        case 'y':
            terminal_writestring("y");
            buffer[inc] = 'y';
             break;
        case 'z':
            terminal_writestring("z");
            buffer[inc] = 'z';
            break;
        case 0x0e :
           dec_col();
           terminal_writestring(" ");
           dec_col();
           buffer[inc-1] = '\0';
           inc -= 1;
           break;
        case ' ':
            terminal_writestring(" ");
            buffer[inc] = ' ';
            break;
        case 0x1c:
            running = false;
            break;
        
    }
        delay(350);
        inc++;
    }
}




#endif
