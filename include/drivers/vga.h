

#ifndef vga_h
#define vga_h

//                                              VGA DRIVERS
//                                SRC: http://wiki.osdev.org/Bare_Bones
//                           MODIFIED JAKE HAYGOOD TO INCLUDE SCROLL FUNCTION
//------------------------------------------------------------------------------------------------------------------------
/* Hardware text mode color constants. */
enum vga_color {
    VGA_COLOR_BLACK = 0,
    VGA_COLOR_BLUE = 1,
    VGA_COLOR_GREEN = 2,
    VGA_COLOR_CYAN = 3,
    VGA_COLOR_RED = 4,
    VGA_COLOR_MAGENTA = 5,
    VGA_COLOR_BROWN = 6,
    VGA_COLOR_LIGHT_GREY = 7,
    VGA_COLOR_DARK_GREY = 8,
    VGA_COLOR_LIGHT_BLUE = 9,
    VGA_COLOR_LIGHT_GREEN = 10,
    VGA_COLOR_LIGHT_CYAN = 11,
    VGA_COLOR_LIGHT_RED = 12,
    VGA_COLOR_LIGHT_MAGENTA = 13,
    VGA_COLOR_LIGHT_BROWN = 14,
    VGA_COLOR_WHITE = 15,
};

inline uint8_t vga_entry_color(enum vga_color fg, enum vga_color bg) {
    return fg | bg << 4;
}

inline uint16_t vga_entry(unsigned char uc, uint8_t color) {
    return (uint16_t) uc | (uint16_t) color << 8;
}


size_t strlen(const char* str) {
    size_t len = 0;
    while (str[len])
        len++;
    return len;
}

static const size_t VGA_WIDTH = 80;
static const size_t VGA_HEIGHT = 25;

size_t terminal_row;
size_t terminal_column;
uint8_t terminal_color;
uint16_t* terminal_buffer;
const char* prompt = "SYSTEM";
void terminal_initialize(void) {
    terminal_row = 0;
    terminal_column = 0;
    terminal_color = vga_entry_color(VGA_COLOR_GREEN, VGA_COLOR_BLACK);
    terminal_buffer = (uint16_t*) 0xB8000;
    for (size_t y = 0; y < VGA_HEIGHT; y++) {
        for (size_t x = 0; x < VGA_WIDTH; x++) {
            const size_t index = y * VGA_WIDTH + x;
            terminal_buffer[index] = vga_entry(' ', terminal_color);
        }
    }
}
void newline()
{
    terminal_row++;
    terminal_column = 0;
    if(terminal_row == VGA_HEIGHT)
    {
        scroll();
    }
    
}

void terminal_setcolor(uint8_t color) {
    terminal_color = color;
}

void terminal_putentryat(char c, uint8_t color, size_t x, size_t y) {
    const size_t index = y * VGA_WIDTH + x;
    terminal_buffer[index] = vga_entry(c, color);
}

void terminal_putchar(char c) {
    terminal_putentryat(c, terminal_color, terminal_column, terminal_row);
    if (++terminal_column == VGA_WIDTH) {
        newline();
    }
}
void dec_col()
{
    terminal_column -= 1;
}
void inc_col()
{
    terminal_column += 1;
}
void dec_row()
{
    terminal_row -= 1;
}
void inc_row()
{
    terminal_column += 1;
}
void set_row(int row)
{
    terminal_row = row;
}
void set_col(int col)
{
    terminal_column = col;
}
void terminal_write(const char* data, size_t size) {
    for (size_t i = 0; i < size; i++)
        if (data[i] == "\n")
        {
            terminal_row++;
            terminal_column = 0;
        }else{
            terminal_putchar(data[i]);
        }
}
//scroll function by Jake Haygood
void scroll()
{
    uint16_t terminal_buffer_temp[VGA_WIDTH][VGA_HEIGHT];
    for(int x = 0; x < VGA_WIDTH; x++)
    {
        for(int y = 0; y < VGA_HEIGHT; y++)
        {
            terminal_buffer_temp[x][y] = terminal_buffer[y * VGA_WIDTH + x];
        }
    }
    terminal_clear();
    for(int x = 0; x < VGA_WIDTH; x++)
    {
        for(int y = 0; y < VGA_HEIGHT-1; y++)
        {
            terminal_buffer[y * VGA_WIDTH + x] = terminal_buffer_temp[x][y+1];
           
        }
    }
    terminal_row=VGA_HEIGHT-1;
    terminal_column=0;
    
}

void terminal_clear()
{
    for (int x = 0; x < VGA_WIDTH; x++)
        for(int y = 0; y < VGA_HEIGHT; y++)
        {
            terminal_putentryat(" ", VGA_COLOR_BLACK, x, y);
        }
}
void terminal_writestring(const char* data) {
    terminal_write(data, strlen(data));
}
//------------------------------------------------------------------------------------------------------------------------
#endif /* vga_h */


