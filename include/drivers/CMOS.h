//                                           CMOS TIME DRIVER
//                                     CREATED BY JAKE HAYGOOD 2017
//----------------------------------------------------------------------------------------------------
#ifndef time_h
#define time_h
#include "portio.h"
//----------------------------------------------------------------------------------------------------
//This is still very buggy
int getSeconds()
{

    while(1)
    {
       outb(0x70, 0x0b);
       if(!(inb(0x71) & 80))
       {
           outb(0x70, 0x00);
           return inb(0x71);
       }
    }

}
int getMinutes()
{
    while(1)
    {
        outb(0x70, 0x0b);
        if(!(inb(0x71) & 80))
        {
            outb(0x70, 0x02);
            return inb(0x71);
        }
    }
}
int getHours()
{
    while(1)
    {
        outb(0x70, 0x0b);
        if(!(inb(0x71) & 80))
        {
            outb(0x70, 0x04);
            return inb(0x71);
        }
    }
}
int getDayOfWeek()
{
    while(1)
    {
        outb(0x70, 0x0b);
        if(!(inb(0x71) & 80))
        {
            outb(0x70, 0x06);
            return inb(0x71);
        }
    }
}
int getDayOfMonth()
{
    while(1)
    {
        outb(0x70, 0x0b);
        if(!(inb(0x71) & 80))
        {
            outb(0x70, 0x07);
            return inb(0x71);
        }
    }
}
int getCenturyLow()
{
    while(1)
    {
        outb(0x70, 0x0b);
        if(!(inb(0x71) & 80))
        {
            outb(0x70, 0x09);
            return inb(0x71);
        }
    }
}
int getCenturyHigh()
{
    while(1)
    {
        outb(0x70, 0x0b);
        if(!(inb(0x71) & 80))
        {
            outb(0x70, 0x32);
            return inb(0x71);
        }
    }
}
int set_time(int h, int m, int s)
{
    outb(0x70, 0x04);
    outb(0x71, h);
    outb(0x70, 0x02);
    outb(0x71, m);
    outb(0x70, 0x00);
    outb(0x71, s);
}

#endif /* time_h */
