//                                            BASIC MATH LIBRARY
//                                        CREATED BY JAKE HAYGOOD 2017
//------------------------------------------------------------------------------------------------------------------------

#ifndef math_h
#define math_h

int power(int base, int exp)
{
    int out = base;
    
    for (int i = 1; i < exp; i ++)
    {
        out = out * base;
    }
    if(exp == 0)
    {
        return 1;
    }else{
        return out;
    }
}
int BCDToInt(int BCDNumber)
{
    return BCDNumber - 6 * (BCDNumber >> 4);
}

int getDigits(int integer)
{
    int inc = 0;
    while (integer > 0)
    {
        inc ++;
        integer = integer / 10;
    }
    return inc;
}

double factorial(double x)
{
    int retval = x;
    while(x > 1)
    {
        retval = retval * (retval-1);
    }
    return retval;
}

double sin(float x)
{
    double subval = 0;
    int precision = 3;
    for (int i = 1; i < precision+1; i++)
    {
        subval += (power(x, 3+(i-1)*2))/(factorial(i*3));
    }
    return x - subval;
}




#endif /* math_h */
